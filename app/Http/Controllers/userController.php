<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\User;
use App\akun;

class userController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$user = User::all();
        $data['user'] = $user;
        $data['headertitle'] = 'Daftar Pengguna';
    	return view('dashboard.pages.user.user', $data);
    }

    public function index1()
    {
        $user = akun::all();
        $data['user'] = $user;
        $data['headertitle'] = 'Profil Anda';
        return view('dashboard.pages.profil.profil', $data);
    }

    public function create()
    {
    	return view('dashboard.pages.user.user');
    }

    public function store(Request $request)
    {
        $request->validate([
                'nama' =>'required',
                'edukasi' => 'required',
                'lokasi' => 'required',
                'skill' => 'required',
                'note' => 'required'
        ]);
        $update = akun::create([
         'nama' => $request['nama'],
         'edukasi' => $request['edukasi'],
         'lokasi' => $request['lokasi'],
         'skill' => $request['skill'],
         'note' => $request['note'],
         "user_id" => Auth::id()
        ]);
        return redirect('/profil/'.$id)->with('success', 'Akun Berhasil Diupdate');
    }

    public function show($id)
    {
        $user = akun::all();
        $data['user'] = $user;
        $data['headertitle'] = 'Profil Anda';
        return view('dashboard.pages.profil', $data);
    }

    public function show1($id)
    {
        $user = User::find($id);
        $data['user'] = $user;
        $data['headertitle'] = 'Profil';
        // dd($user->profile);
        return view('dashboard.pages.profil.show', $data);
    }

    public function edit($id)
    {
        $akun = akun::find($id);
        $data['akun'] = $akun;
        $data['headertitle'] = 'Edit Your Acount';
    	return view('dashboard.pages.profil.edit', $data);
    }

    public function update($id, Request $request)
    {
    	$request->validate([
    			'nama' =>'required',
    			'edukasi' => 'required',
    			'lokasi' => 'required',
                'skill' => 'required',
                'note' => 'required'
    	]);
        $update = akun::where('id', $id)->update([
         'nama' => $request['nama'],
         'edukasi' => $request['edukasi'],
         'lokasi' => $request['lokasi'],
         'skill' => $request['skill'],
         'note' => $request['note']
        ]);
    	return redirect('/profil/{id}')->with('success', 'Akun Berhasil Diupdate');
    }

    public function destroy($id)
    {
        akun::destroy($id);
    	return redirect('/profil')->with('success', 'Data Diri Anda Berhasil Dihapus');
    }

    public function myprofile()
    {
        $id = Auth::id();
        $user = User::find($id);
        $data['user'] = $user;
        $data['headertitle'] = 'Profil Saya';
        return view('dashboard.pages.profil.myprofile', $data);
    }
}

