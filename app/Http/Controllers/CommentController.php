<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Comment;
use App\User;

class CommentController extends Controller
{
    //

    public function index()
    {
        # code...
    }

    public function store(Request $request)
    {
        $id = Auth::id();
        if(empty($id)){
            return back()->with('error','Please Login!');
        }
        
        $request->validate([
            'comments' => 'required'
        ]);

        $input['comment'] = $request->comments;
        $input['article_id'] = $request->article_id;
        $input['user_id'] = $id;

        $query = Comment::create($input);

        return back()->with('success','Success Comment');
    }

    public function destroy($id)
    {
        $query = Comment::where('id',$id)->delete();

        return redirect()->back()->with('successDelete','Deleted Comment Success');
    }
}
