<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Article;
use App\User;
use App\Tag;
Use Alert;


class ArticleController extends Controller
{
    public function __construct(){

        $this->middleware('auth')->except('article','showArticle');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $article = Article::all();

        $data['article'] = $article;
        $data['headertitle'] = 'Articles';

        return view('dashboard.pages.article.article-table', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['headertitle'] = 'Create Article';

        return view('dashboard.pages.article.article-create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'thumbnail' => 'required|image|file|max:7000',
            'title' => 'required',
            'article' => 'required'
        ]);

        $input['title'] = $request->title;
        $input['article'] = $request->article;
        $input['user_id'] = Auth::id();

        $tags = $request->tags;
        // Array Tags
        $tags_arr = explode(',', $tags);
        
        $tag_ids = [];
        foreach ($tags_arr as $tag_name ) {
            $tag = Tag::where('tag_name', $tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id;
            }else{
                $new_tag = Tag::create(['tag_name'=>$tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }

        // Name File
        $namefile = str_replace(' ', '_', $request->title)."-".date('YmdHis').".".$request->file('thumbnail')->getClientOriginalExtension();
        // Upload File       
        $path = Storage::putFileAs(
            'public/thumbnail', $request->file('thumbnail'), $namefile
        );

        $input['thumbnail'] = $namefile;

        $action = Article::create($input);

        $action->tags()->sync($tag_ids);

        Alert::success('Success', 'Create Article Success');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::find($id);
        if($article->user_id==Auth::id()){
            $tags = $article->tags;
            $data['article'] = $article;
            $data['tags'] = $tags;
            $data['headertitle'] = 'Edit Article';
            
            return view('dashboard.pages.article.article-edit', $data);
        }else{
            Alert::error('Error', 'Somenthing was wrong!');
            return redirect('/article');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'thumbnail' => 'image|file|max:7000',
            'title' => 'required',
            'article' => 'required'
        ]);
        
        if($request->hasFile('thumbnail')){
            // Name File
            $namefile = str_replace(' ', '_', $request->title)."-".date('YmdHis').".".$request->file('thumbnail')->getClientOriginalExtension();
            // Upload File       
            $path = Storage::putFileAs(
                'public/thumbnail', $request->file('thumbnail'), $namefile
            );
            $update['thumbnail'] = $namefile;

        }

        $tags = $request->tags;
        // Array Tags
        $tags_arr = explode(',', $tags);
        
        $tag_ids = [];
        foreach ($tags_arr as $tag_name ) {
            $tag = Tag::where('tag_name', $tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id;
            }else{
                $new_tag = Tag::create(['tag_name'=>$tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }

        $id_pivot = explode(',', $request->pivot);

        $update['title'] = $request->title;
        $update['article'] = $request->article;
        
        $action = Article::where('id', $id)->update($update);

        // $action->tags()->sync($tag_ids);
        // $action->tags()->newPivotStatement()->where('article_id', $id)->update($tag_ids);
        // $action->tags()->updateExistingPivot($id_pivot, $tag_ids);
        Alert::success('Article Updated', 'Success Update Article!');

        return redirect('/article/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $delete = Article::where('id', $id)->delete();
        
        Alert::success('Deleted', 'Article Deleted!');
        return redirect()->back()->with('success','Deleted Success');
    }

    public function article()
    {
        $article = Article::all();

        $data['articles'] = $article;
        $data['headertitle'] = 'News & Article';

        return view('front-page.pages.article-card', $data);
    }

    public function showArticle($id)
    {
        $article = Article::find($id);

        $data['article'] = $article;
        $data['headertitle'] = 'News & Article';

        return view('front-page.pages.article', $data);
    }

    public function examples(){
        return view ('home');
    }
}

