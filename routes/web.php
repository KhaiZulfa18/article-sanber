<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/article', 'ArticleController');

Auth::routes();

Route::get('/home', 'ArticleController@article');
Route::get('/home/{id}', 'ArticleController@showArticle');

Route::get('/user/{id}', 'userController@index');
Route::get('/user/{id}/show', 'userController@show1');

Route::get('/profil', 'userController@index1');
Route::post('/profil', 'userController@store');
Route::get('/profil/{id}', 'userController@show');
Route::get('/profil/{id}/edit', 'userController@edit');
Route::put('/profil/{id}', 'userController@update');
Route::delete('/profil/{id}', 'userController@destroy');

Route::get('/artikel', 'ArticleController@examples');

Route::post('/comments', 'CommentController@store');
Route::delete('/comments/{id}', 'CommentController@destroy')->name('comment.destroy');

Route::get('/myprofile', 'userController@myprofile');
