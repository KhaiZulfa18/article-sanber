@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            
            <div class="mt-5"> 
                <h1> <b>Pengertian dan Sejarah Framework Laravel</b> </h1>
                <p class="ml-4 mb-5 text-muted">September 2020</p>
                <div class="article-content" id="jenisfont">
                    <div class="entry-content">
                    <div>   <!-- PENGERTIAN LARAVEL -->
                    <h3 class="mb-3"> <b>Pengertian Laravel</b> </h3>
                    <p class="text-justify">
                    <strong>Laravel</strong> 
                    adalah framework berbasis PHP yang dibangun dengan konsep MVC 
                    (model view controller), yang dikembangkan oleh Taylor Otwell  pada tahun 2011. 
                    Laravel adalah sebuah framework PHP yang dirilis dibawah lisensi MIT,
                    yang diranceng untuk meningkatkan pengalaman bekerja dengan aplikasi dengan 
                    menyediakan sintaks yang ekspresif, jelas dan dapat menghemat waktu. 
                    </p>
                    <p class="mb-0">
                    <strong>MVC</strong> 
                    adalah sebuah pendekatan perangkat lunak yang memisahkan aplikasi logika dari 
                    presentasi.MVC memisahkan aplikasi berdasarkan komponen- komponen aplikasi,
                    seperti : manipulasi data, controller, dan user interface.
                    </p>
                    <ol>
                        <li>
                        <i>Model</i>, Model mewakili struktur data. Biasanya model berisi fungsi-fungsi 
                        yang membantu seseorang dalam pengelolaan basis data seperti memasukkan data 
                        ke basis data, pembaruan data dan lain-lain.
                        </li>
                        <li>
                        <i>View</i>, View adalah bagian yang mengatur tampilan ke pengguna. 
                        Bisa dikatakan berupa halaman web.
                        </li>
                        <li>
                        <i>Controller</i>, Controller merupakan bagian yang menjembatani model dan view.
                        </li>
                    </ol>
                    </div>
                    <div>   <!-- FITUR FITUR LARAVEL -->
                        <h4 class="mt-4 ml-2"><b>Fitur-Fitur Laravel</b></h4>
                        <p class="mb-0 ml-2">
                        Banyak sekali fitur yang tersedia pada PHP Framework modern ini, Berikut ini
                        fitur framework Laravel yang membedakan antara framework php ini dari
                        framework lainnya.
                        </p>
                        <ol class="ml-2">
                            <li>
                            <em>Dependency Management</em>, Adalah sebuah fitur yang berguna untuk memahami
                            fungsi wadah layanan (IoC) memungkin objek baru dihasilkan dengan pembalikan
                            controller dan merupakan bagian inti untuk mempelajari aplikasi web modern.
                            </li>
                            <li>
                            <em>Modularity</em>, adalah sejauh mana sebuah komponen aplikasi web dapat dipisahkan
                            dan digabungkan kembali. Modularitas dapat membantu kamu untuk mempermudah proses
                            update, Bukan hanya itu, Dalam membangun dan mengembangkan website, fitur ini memungkinkan
                            kamu untuk menyempurnakan dan meningkatkan fungsionalitas dari web tersebut.
                            </li>
                            <li>
                            <em>Authentication</em>, adalah bagian integral dari aplikasi web modern, Laravel menyediakan 
                            otentikasi di luar kotak,dengan menjalankan perintah sederhana. Kamu juga dapat membuat 
                            sebuah sistem yang otentikasinya berfungsi secara penuh dan proses konfigurasi otentikasi 
                            sudah berjalan secara otomatis.
                            </li>
                            <li>
                            <em>Caching</em>, adalah sebuah teknik untuk menyimpan data di lokasi penyimpanan sementara
                            dan dapat diambil dengan cepat saat dibutuhkan. Dalam laravel, hampir semua data caching 
                            berasal dari tampilan ke rute, Sehingga dapat mengurangi waktu pemrosesan dan meningkatkan kinerja.
                            </li>
                            <li>
                            <em>Routing</em>, Routing Laravel dapat digunakan untuk membuat aplikasi yang tenang dengan mudah.
                            Dalam framework ini semua request dipetakan dengan bantuan rute. Kamu juga dapat mengelompokkan rute,
                            menamainya, menerapkan filter pada mereka dan mengikat data model Anda kepadanya.
                            </li>
                            <li>
                            <em>Restful Controller</em>, Adalah sebuah fitur yang berfungsi memisahkan logika dalam melayani 
                            HTTP GET and POST.Kamu juga dapat membuat pengontrol sumber daya yang dapat digunakan untuk 
                            membuat CRUD dengan mudah.
                            </li>
                            <li>
                            <i>Testing and Debugging</i>, Laravel hadir dengan menggunakan PHPUnit di luar kotak yang berfungsi 
                            untuk menguji aplikasi kamu. Framework ini dibangun dengan pengujian dalam pikiran sehingga mendukung 
                            pengujian dan debugging terlalu banyak.
                            </li>
                            <li>
                            <em>Template Engemne</em>, Blade adalah template engine Laravel, Blade berfungsi untuk menyediakan sejumlah 
                            fungsi pembantu untuk memformat data kamu dalam bentuk tampilan.
                            </li>
                            <li>
                            <i>Configuration Management Features</i>, Dalam laravel semua file konfigurasi kamu disimpan di dalam  
                            direktori config, Kamu dapat mengubah setiap konfigurasi yang tersedia.
                            </li>
                            <li>
                            <i>Eloquent ORM</i>, Laravel berbasis pada Eloquent ORM yang menyediakan dukungan untuk hampir semua mesin 
                            basis data. Fitur ini juga berfungsi untuk menjalankan berbagai operasi database di dalam website 
                            dan mendukung berbagai sistem database seperti MySQL dan SQLite.
                            </li>
                        </ol>
                        <p> 
                        Itulah beberapa fitur penting yang hanya dimiliki oleh Framework Laravel yang membuat 
                        Laravel ini spesial dibanding Framework PHP yang lain.
                        </p>
                    </div>
                    <div>   <!-- SEJARAH LARAVEL -->
                        <h3 mt-6> <b>Sejarah Laravel</b> </h3>
                        <ul>
                        <li>
                        Sejarah Framework Laravel pertama kali dibuat oleh Taylor Otwell. Laravel diciptakan oleh
                        Taylor Otwell untuk memberikan alternatif yang lebih baik dari Framework PHP yang yang
                        lain seperti Codeigniter. Framework Laravel di perkenalkan pertama kali pada 09 juni 2011
                        dengan versi beta. Masih dibulan yang sama Laravel merilis versi pertamanya yaitu Laravel
                        1, pada Laravel 1 ini sudah dibekali banyak fitur diantaranya authentication, localisation,
                        models, views, sessions, routing dan fitur-fitur lainya, tetapi pada Laravel 1 masih kurang
                        mendukung untuk Controller. Pada versi ini Laravel belum menggunakan konsep MVC.
                        </li>
                        <br>
                        <li>
                        Selanjutnya Laravel 2, versi ini di rilis pada bulan September 2011. Banyak terjadi penigkatan
                        pada Laravel 2, fitur baru utamanya adalah sudah mendukung Controller. Fitur ini yang membuat
                        Laravel menjadi Framework MVC sepenuhnya. Apa itu MVC? Nanti akan saya bahas. Selanjutnya fitur
                        yang baru yang ga kalah keren adalah templating atau yang sering disebut blade.
                        </li>
                        <br>
                        <li>
                        Laravel 3 dirilis pada Februari 2012. Pada Laravel 3 mempunyai fitur – fitur baru diantaranya
                        Command-Line Interface (CLI) atau yang biasa disebut Artisan. Pada versi ini juga sudah mendukung
                        database migrations, dan packaging system atau yang sering kita kenal dengan  Bundles.
                        </li>
                        <br>
                        <li>
                        Laravel 4 dirilis pada Mei 2014, dengan sebutan Illuminate. Pada versi ini Laravel sudah menggunakan 
                        Composer sebagai package managernya.
                        </li>
                        </ul>
                    </div>
                    <!-- TABEL RILIS LARAVEL -->
                    <div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable dtr-inline collapsed" role="grid" aria-describedby="example2_info">
                        <thead id="headtabel">
                        <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">Version</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Release</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Perbaikan BUG</th><th class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Perbaikan Keamanan</th></tr>
                        </thead>
                        <tbody id=bodytabel>
                        <tr role="row" class="odd">
                        <td tabindex="0" class="sorting_1"> <strong>V1</strong> </td>
                        <td> <strong>Juni 2011</strong> </td>
                        <td>-</td>
                        <td>-</td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td tabindex="0" class="sorting_1"> <strong>V2</strong> </td>
                        <td> <strong>September 2011</strong> </td>
                        <td>-</td>
                        <td>-</td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="odd">
                        <td tabindex="0" class="sorting_1"> <strong>V3</strong> </td>
                        <td> <strong>Februari 2012</strong> </td>
                        <td>-</td>
                        <td>-</td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td tabindex="0" class="sorting_1"> <strong>V4</strong> </td>
                        <td> <strong>Mei 2013</strong> </td>
                        <td>-</td>
                        <td>-</td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="odd">
                        <td class="sorting_1" tabindex="0"> <strong>5.0</strong> </td>
                        <td> <strong>4 Februari 2015</strong> </td>
                        <td> <strong>4 Agustus 2015</strong> </td>
                        <td> <strong>4 Februari 2016</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.1 (LTS)</strong> </td>
                        <td> <strong>09 Juni 2015</strong> </td>
                        <td> <strong>09 Juni 2017</strong> </td>
                        <td> <strong>09 Juni 2018</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="odd">
                        <td class="sorting_1" tabindex="0"> <strong>5.2</strong> </td>
                        <td> <strong>21 Desember 2015</strong> </td>
                        <td> <strong>21 Juni 2016</strong> </td>
                        <td> <strong>21 Desember 2016</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.3</strong> </td>
                        <td> <strong>23 Agustus 2016</strong> </td>
                        <td> <strong>23 Februari 2017</strong> </td>
                        <td> <strong>23 Agustus 2017</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="odd">
                        <td class="sorting_1" tabindex="0"> <strong>5.4</strong> </td>
                        <td> <strong>24 Januari 2017</strong> </td>
                        <td> <strong>24 Juli 2017</strong> </td>
                        <td> <strong>24 Januari 2018</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.5 (LTS)</strong> </td>
                        <td> <strong>30 Agustus 2017</strong> </td>
                        <td> <strong>30 Agustus 2019</strong> </td>
                        <td> <strong>30 Agustus 2020</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.6</strong> </td>
                        <td> <strong>7 Februari 2018</strong> </td>
                        <td> <strong>7 Agustus 2018</strong> </td>
                        <td> <strong>7 Februari 2018</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.7</strong> </td>
                        <td> <strong>4 September 2018</strong> </td>
                        <td> <strong>4 Februari 2019</strong> </td>
                        <td> <strong>4 September 2019</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>5.8</strong> </td>
                        <td> <strong>26 Februari 2019</strong> </td>
                        <td> <strong>26 Agustus 2019</strong> </td>
                        <td> <strong>26 Februari 2020</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>6.0 (LTS)</strong> </td>
                        <td> <strong>3 September 2019</strong> </td>
                        <td> <strong>3 September 2021</strong> </td>
                        <td> <strong>3 September 2022</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>7</strong> </td>
                        <td> <strong>3 Maret 2020</strong> </td>
                        <td> <strong>3 September 2020</strong> </td>
                        <td> <strong>3 Maret 2021</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        <tr role="row" class="even">
                        <td class="sorting_1" tabindex="0"> <strong>8</strong> </td>
                        <td> <strong>8 September 2020</strong> </td>
                        <td> <strong>8 Fabruari 2021</strong> </td>
                        <td> <strong>8 September 2021</strong> </td>
                        <td style="display: none;">A</td>
                        </tr>
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- KEPERLUAN CSS  -->
    <style>
    #jenisfont{
        font-family:sans-serif;
        font-size:17px;
    }

    #headtabel{
        background-color:#6015ab;
        color:white;
        font-family:monospace;
        font-size:14px;
    }

    #bodytabel{
        font-family:monospace;
        font-size:14px;
    }

    .table-hover tbody tr:hover td {
        background-color:#6015ab;
        color:white;
    }
    </style>

</div>


@endsection
