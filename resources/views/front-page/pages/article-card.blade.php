@extends('front-page.layouts.master')

@section('first-section')
<div class="first-section">
    <div class="container">
        <div class="row justify-content-center align-items-center" style="height: 50vh;">
            <div class="col-md-8 text-center">
                <h1 class="display-4">News & Articles</h1>
                <p class="lead text-light-gray ">Blog is another word for web-blog.</p>
                <div>
                    <div class="form-group d-flex justify-content-between align-items-center">
                        <input type="text" class="form-control form-control-lg mr-3" placeholder="Search card blog">
                        <button class="btn btn-primary d-flex align-items-center btn-lg">
                            <i class="fa fa-search mr-2" aria-hidden="true"></i>
                            Search
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('second-section')
<div class="second-section" >
    <div class="container">
        <div class="row mb-5">
            @foreach ($articles as $article)
                
            <div class="col-md-4">
                <div class="card shadow-sm" >
                    <img src="{{ asset("storage/thumbnail/{$article->thumbnail}") }}" class="card-img-top shadow" alt="{{ $article->title }}">
                    <div class="card-body">
                        @foreach ($article->tags as $tag)
                        <span class="badge badge-light-primary mb-3">{{ $tag->tag_name }}</span>
                        @endforeach
                        <h5 class="card-title">{{ $article->title }}</h5>
                        <div class="card-text">{!! Str::limit($article->article, 50, '...') !!}</div>
                        <div class="d-flex justify-content-between align-items-center mt-3">
                            <div class="media ">
                                <a href="{{ url('/home/'.$article->id) }}" class="btn btn-outline-info"> Read More....</a>
                            </div>
                            <span class="text-muted">{{ $article->created_at }}</span>
                        </div>
                    </div>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div>    
@endsection