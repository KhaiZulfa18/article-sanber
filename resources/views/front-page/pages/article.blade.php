@extends('front-page.layouts.master')

@section('first-section')
<div class="first-section">
    <div class="container" >
        <div class="row justify-content-center">
            <div class="col-md-9 ">
                <h1 >{{ $article->title }}</h1>
                <div class="user d-flex align-items-start justify-content-between bg-light p-4 rounded">
                    <div class="d-flex align-items-start">
                        <div class="d-block">
                            <span class="d-block">by <a class="h6" href="#">{{ $article->user->name }}</a></span>
                            <span class="d-block text-muted">{{ $article->created_at }}</span>
                        </div>
                    </div>
                    <div class="d-flex align-items-end">
                    @foreach ($article->tags as $tag)
                        <span class="badge badge-light-primary mr-2">{{ $tag->tag_name }}</span>
                    @endforeach
                    </div>
                </div>
                <!--//blog section-->
                <div class="blog-section">
                    <img src="{{ asset("storage/thumbnail/{$article->thumbnail}") }}" class="bounce-little img-fluid shadow-sm mt-4" alt="{{ $article->title }}">

                    <div class="mt-4">{!! $article->article !!}</div>

                </div>
                <!--share section-->
                <hr>
                <div class="share text-center d-flex align-items-center justify-content-center">
                    <span class="mr-3">Share this article:</span>
                    <i class="fa fa-facebook-square text-primary" aria-hidden="true"></i>
                    <i class="fa fa-google-plus text-primary" aria-hidden="true"></i>
                    <i class="fa fa-github text-primary"  aria-hidden="true"></i>
                </div>
                <hr>

                <!--comments section-->
                <div class="comment-section">
                    <h4>{{ count($article->comments)}} Comments</h4>
                    @foreach ($article->comments as $comment)
                    <div class="media">
                        <button class="btn btn-light-info text-center rounded-circle mr-4">
                            <i class="fa fa-comments rounded-circle"></i>
                        </button>
                        <div class="media-body">
                            <h6 class="mt-0">{{$comment->user->name }}<span class="ml-3 text-muted small">{{ $comment->created_at }}</span></h6>
                            {{ $comment->comment}}
                        </div>
                        <div class="reply">
                            @if ($comment->user_id==Auth::id())
                            
                                <a class="text-danger" href="{{ route('comment.destroy',$comment->id) }}" onclick="event.preventDefault(); document.getElementById('delete-comment-form').submit();">Delete Comment</a>
                            @elseif($article->user_id==Auth::id())
                                <a class="text-danger" href="{{ route('comment.destroy',$comment->id) }}" onclick="event.preventDefault(); document.getElementById('delete-comment-form').submit();">Delete Comment</a>
                            @endif
                            <form id="delete-comment-form" action="{{ route('comment.destroy',$comment->id) }}" method="POST" style="display: none;">
                                @csrf
                                @method('DELETE')
                            </form>
                        </div>
                    </div>   
                    @endforeach
                    <hr>
                    <div class="post-comment">
                        <h1 class="h2">Post a comment</h1>
                        <form action="{{ url('/comments')}}" method="POST">
                            @csrf
                            <div class="form-row">
                                <div class="col">
                                    <input type="hidden" class="form-control" name="article_id" placeholder="Name" value="{{ $article->id }}">
                                </div>
                            </div>
                            <div class="form-group mt-3">
                                <textarea class="form-control" name="comments" cols="10" rows="5" placeholder="Comment here."></textarea>
                                <span class="text-danger">{{ $errors->first('comments') }}</span>
                            </div>
                            <div class="d-flex justify-content-between">
                                @if (session('success'))
                                <div class="alert alert-success">
                                    {{ session('success') }}
                                </div>
                                @endif
                                @if (session('successDelete'))
                                <div class="alert alert-success">
                                    {{ session('successDelete') }}
                                </div>
                                @endif
                                @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                                @guest
                                <button class="btn btn-primary" onclick="alert('Please Login!')">Post</button>
                                @endguest
                                @auth
                                <button type="submit" class="btn btn-primary">Post</button>
                                @endauth
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection