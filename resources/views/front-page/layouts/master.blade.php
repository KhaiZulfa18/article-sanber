<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Article</title>
    <link rel="stylesheet" href="{{ asset('template/front-page/css/apollo.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front-page/css/style.card.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front-page/css/pensive.css') }}">
    <link rel="stylesheet" href="{{ asset('template/front-page/css/style.article.css') }}">
    <!-- font awesome icons-->
    {{-- <link href="{{ asset('template/fontawesome/css/font-awesome.min.css') }}" > --}}
    {{-- <link href="{{ asset('template/fontawesome/css/all.css') }}" > --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<!--    navbar start-->
@include('front-page.layouts.navbar')
<!--navbar ends-->
<!-- start first section-->
@yield('first-section')
<!--end first-section-->

{{-- second section --}}
@yield('second-section')
{{-- end second section --}}

{{-- Footer --}}
<div class="fourth-section ">
    <hr>
    <div class="container mt-4">
        <p class="text-center"> © Bootcatch 2020 Copyright All rights reserved.</p>
    </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{ asset('template/front-page/js/jquery-3.4.1.slim.min.js') }}" ></script>
<script src="{{ asset('template/front-page/js/popper.min.js') }}" ></script>
<script src="{{ asset('template/front-page/js/bootstrap.min.js') }}" ></script>
</body>
</html>
