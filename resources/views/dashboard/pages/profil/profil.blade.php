@extends('dashboard.layouts.master')

@section('content')
		<div class="m-3">
			<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Tambah Akun</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/profil" method="POST">
              	@csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama" name="nama" value=" {{old('nama', '')}} " placeholder="Nama Lengkap">
                    @error('nama')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                  <div class="form-group">
                    <label for="edukasi">Education</label>
                    <input type="text" class="form-control" id="edukasi" name="edukasi" value=" {{old('edukasi', '')}} " placeholder="edukasi">
                    @error('edukasi')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                  <div class="form-group">
                    <label for="lokasi">Location</label>
                    <input type="text" class="form-control" id="lokasi" name="lokasi" value=" {{old('lokasi', '')}} ">
                    @error('lokasi')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                  <div class="form-group">
                    <label for="skill">Skills</label>
                    <input type="text" class="form-control" id="skill" name="skill" value=" {{old('skill', '')}} ">
                    @error('skill')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
                  </div>
                  <div class="form-group">
                    <label for="note">Note</label>
                    <input type="text" class="form-control" id="note" name="note" value=" {{old('note', '')}} ">
                    @error('note')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Tambah</button>
                  <a href="/profil/{{Auth::user()->id}}" class="btn btn-info btn-sm">show</a>
                </div>
              </form>
            </div>
        </div>

@endsection