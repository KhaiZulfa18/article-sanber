@extends('dashboard.layouts.master')
@push('css')
<link rel="stylesheet" href="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.min.css') }}">
@endpush

@section('content')

		<div class="m-3">	
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">Tabel Pengguna</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                @if(session('success'))
                  <div class="alert alert-success">
                    {{session('success')}}
                  </div>
                @endif
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">No</th>
                      <th>Usename</th>
                      <th>Email</th>
                      <th style="width: 40px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($user as $key => $dat)
                    	<tr>
                    		<td> {{$key + 1}} </td>
                    		<td> {{$dat->name}} </td>
                    		<td> {{$dat->email}} </td>
                    		<td style="display: flex;">
                            <a href="/user/{{$dat->id}}/show" class="btn btn-info btn-sm">show</a>
                        </td>
                    	</tr>
                    @empty
                    <tr>
                      <td colspan="12" align="center">No Data</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
            </div>
		</div>

@endsection