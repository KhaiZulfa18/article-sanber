@extends('dashboard.layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.min.css') }}">
@endpush

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Article</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/artikel">Article</a></li>
              <li class="breadcrumb-item active">Create Article</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3>Create Article</h3>
            </div>
            <form role="form" action="{{ route('article.update', $article->id)}}" method="post" enctype='multipart/form-data'>
                @csrf
                @method('PUT')
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control col-md-6" id="title" name="title" placeholder="Title" value="{{ $article->title }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <input type="text" class="form-control col-md-6" id="tags" name="tags" placeholder="Tags *use ',' to separate tags (eg. PHP, JavaScript, Python )" value="@foreach($tags as $tag){{$tag->tag_name}},@endforeach">
                        <span class="text-danger">{{ $errors->first('tags') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="tags">Thumbnail</label>
                        <input type="file" class="form-control-file col-md-6 " id="thumbnail" name="thumbnail" >
                        <span class="text-muted">*empty if you don't want to change the thumbnail</span>
                        <span class="text-danger">{{ $errors->first('thumbnail') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="article">Article</label>
                        <textarea id="summernote" name="article">{{ $article->article }}</textarea>
                        <span class="text-danger">{{ $errors->first('article') }}</span>
                    </div>
                    <div class="form-group">
                    <input type="hidden" id="pivot" name="pivot" value="@foreach($article->tag as $pivot) {{ $pivot->id }} @endforeach">
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success')}}
                        </div>
                        @endif
                        @if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error')}}
                        </div>
                        @endif
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
            </form>
        </div>
    </section>
    <!-- /.content -->
@endsection
    
@push('script')
<script src="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
$(function () {
    $("#summernote").summernote({
       placeholder : "Article",
       toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']]
        ],
        height: 300,
        minHeight: null,
        maxHeight: null,
        focus: true 
    });
});
</script>
@endpush