@extends('dashboard.layouts.master')

@push('css')
  <link rel="stylesheet" href="{{ asset('template/dashboard/adminlte/plugins/sweetalert2/sweetalert2.all.min.js')}}">
@endpush

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Article List</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/artikel">Article</a></li>
              <li class="breadcrumb-item active">Article List</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3>Articles</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Article</th>
                            <th>Thumbnail</th>
                            <th>Author</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                           $no = 1; 
                        @endphp
                        @foreach ($article as $row)                            
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $row->title }}</td>
                            <td>{!! Str::limit($row->article, 50, '...') !!}</td>
                            <td>
                              <img src="{{ asset("storage/thumbnail/{$row->thumbnail}") }}" style=" height: 70px;">
                            </td>
                            <td>{{ $row->user->name }}</td>
                            <td class="text-center">
                              <div class="btn-group">
                                <a href="{{ url('/home/'.$row->id)}}" class="btn btn-info"><i class="fas fa-eye"></i></a>
                                @if ($row->user_id == Auth::id())
                                <a href="{{ url('/article/'.$row->id.'/edit') }}" class="btn btn-success"><i class="fas fa-edit"></i></a>
                                <form action="{{ route('article.destroy', $row->id)}}" method="POST">
                                  @csrf
                                  @method('DELETE')
                                  <button type="submit" class="btn btn-danger"><i class="fas fa-trash"></i></button>
                                </form>                                    
                                @endif
                              </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-footer">
            </div>
        </div>
    </section>
    <!-- /.content -->
@endsection

@push('script')
<script src="{{ asset('template/dashboard/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('template/dashboard/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('template/dashboard/adminlte/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>
$(function () {
    $("#example1").DataTable({
        "responsive": true
    });
});
</script>
@endpush