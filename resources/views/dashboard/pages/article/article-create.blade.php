@extends('dashboard.layouts.master')

@push('css')
<link rel="stylesheet" href="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.min.css') }}">
@endpush

@section('content')
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Article</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item mr-5 "><a href="/artikel">Artikel Laravel</a></li>
              
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="card">
            <div class="card-header">
                <h3>Create Article</h3>
            </div>
            <form role="form" action="/article" method="post" enctype='multipart/form-data'>
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control col-md-6" id="title" name="title" placeholder="Title" value="{{ old('title') }}">
                        <span class="text-danger">{{ $errors->first('title') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="tags">Tags</label>
                        <input type="text" class="form-control col-md-6" id="tags" name="tags" placeholder="Tags *use ',' to separate tags (eg. PHP, JavaScript, Python )" value="{{ old('tags') }}">
                        <span class="text-danger">{{ $errors->first('tags') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="tags">Thumbnail</label>
                        <input type="file" class="form-control-file col-md-6 " id="thumbnail" name="thumbnail" >
                        <span class="text-danger">{{ $errors->first('thumbnail') }}</span>
                    </div>
                    <div class="form-group">
                        <label for="article">Article</label>
                        <textarea id="summernote" name="article">{{ old('article') }}</textarea>
                        <span class="text-danger">{{ $errors->first('article') }}</span>
                    </div>
                    <div class="form-group">
                        @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success')}}
                        </div>
                        @endif
                    </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
            </form>
        </div>
    </section>
    <!-- /.content -->
@endsection
    
@push('script')
<script src="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('template/dashboard/adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script>
$(function () {
    $("#summernote").summernote({
       placeholder : "Article",
       toolbar: [
          ['style', ['style']],
          ['font', ['bold', 'underline', 'clear']],
          ['color', ['color']],
          ['para', ['ul', 'ol', 'paragraph']]
        ],
        height: 300,
        minHeight: null,
        maxHeight: null,
        focus: true 
    });
});
</script>
@endpush